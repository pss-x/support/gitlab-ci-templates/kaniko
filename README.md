# PSS®X Container Image Creation Toolset : Kaniko

This template is part of the container creation tools. Template facilitates the use of [kaniko](https://github.com/GoogleContainerTools/kaniko) to provide the following functionality:

* Creation of container images based on Dockerfile(s).
* Pushing images to gitlab container registry.

### Definitions and background

An OCI container image build tool is a piece of software that builds OCI containers. They are built according to the [OCI Image specification](https://github.com/opencontainers/image-spec) and as long as the specification is followed, it enables inoperability.
An OCI build tool builds images by creating a manifest containing metadata about the image content and dependencies with references to the built layers. 
It also contains a configuration that includes information such as application arguments and environments. 
To run the container from its image, its manifest is used to reference and putting together the different layers. 
The configuration is used to configure the function of a specific instance of the container image.

When running OCI build tools inside containers, they often require more privileges then what is by default given to a container. 
Giving extended privileges to a container may increase the attack surface in case of a compromised container.
Therefore, it is important to give containers only the necessary privileges and take precautionary actions if more privileges are required. 
Some build tools require running as the root user to get access to all devices and syscalls, which may also be a security issue in a compromised container.

### Popular build tools

When many people think of building OCI containers, they think of Docker. It is an application that builds and runs OCI containers, but there exist many alternatives. 
One of the benefits of Docker is the use of Dockerfiles, small scripts that create a container image from specific instructions. 
Dockerfiles can extend on other previously built images and even build an image in multiple stages, allowing a small final image footprint. 
Docker cannot run in daemonless mode and since OCI containers have a big problem of running daemons, Docker instead needs to use a separate service container running Docker, making the setup process more tedious.

| Build tool | Frontends | Privileges |User |
| ------------- | ------------- | ------------- | ----------- |
| Docker | Dockerfile | Privileged | Root |
| BuildKit  | Dockerfile | Unconfined SECcomp & AppArmor | Rootless |
| Img  | Dockerfile |Unconfined SECcomp & AppArmor | Rootless |
| Buildah | Dockerfile | Privileged | Rootless |
| Kaniko | Dockerfile | None | Root |
| Jib  | JVM Build system | None | Rootless |
| Umoci | None | N/A | N/A |

Kaniko is a build tool built by developers at Google, which requires no extended privileges but must be run as root. 
It can only use cache to/from an external registry.

## Referencing CI Template

[Gitlab CI includes](https://docs.gitlab.com/ee/ci/yaml/#include) are used for a seamless pipeline intgration.

Include the file with to be extended hidden jobs starting with `extend` from this project

```yaml
include:
  - project: 'pss-x/support/gitlab-ci-templates/kaniko'
    ref: v1.0.5 # select desired version here
    file: '/templates/.kaniko.yml'
```

Include the file with to be extended hidden jobs `extend.gitlab-ci.yml` from this project and use via `extends` in your job:

```yml
use-kaniko:
  extends: .cr:build-container-image
```

RECOMMENDED: use stable version like tag as `ref`.

## TL;DR

Here is an example where an image is created with version tag and pushed to gitlab registry

```yml
include:
  - project: 'pss-x/support/gitlab-ci-templates/releases'
    ref: v1.0.5 # select desired version here
    file: '/templates/changelog.and.releases.yml'
    
  - project: 'pss-x/support/gitlab-ci-templates/kaniko'
    ref: v1.0.5 # select desired version here
    file: '/templates/.kaniko.yml'

stages:
  - version
  - push:cr
  - release

push:cr:
  extends: .cr:build-container-image
  stage: push:cr
  needs: [ "version" ]
  variables:
    IMAGE_TAG: $VERSION
```

Tasks can be run in parallel as well

```yml
...
stages:
  - ...
  - push:cr
  - ...

push:cr-module-1:
  extends: .cr:build-container-image
  stage: push:cr
  variables:
    IMAGE_TAG: "module-1"
    DOCKERFILE: "$CI_PROJECT_DIR/src/GridLab.PSSX.Module1Name/Dockerfile.ModuleName1"
    CONTEXT: "$CI_PROJECT_DIR/src/GridLab.PSSX.Module1Name"
    
push:cr-module-2:
  extends: .cr:build-container-image
  stage: push:cr
  variables:
    IMAGE_TAG: "module-2"
    DOCKERFILE: "$CI_PROJECT_DIR/src/GridLab.PSSX.Module2Name/Dockerfile.ModuleName2"
    CONTEXT: "$CI_PROJECT_DIR/src/GridLab.PSSX.Module2Name"
...
```

Alternatively we can also use [parallel:matrix](https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix)

```yml
push:cr:
  extends: .cr:build-container-image
  stage: push:cr
  variables:
    IMAGE_TAG: $VERSION
  parallel:
    matrix:
      - IMAGE_NAME: "${CI_REGISTRY_IMAGE}/module1"
        DOCKERFILE: "${CI_PROJECT_DIR}/host/GridLab.PSSX.Module1Name/Dockerfile.Module1"
        CONTEXT:    "${CI_PROJECT_DIR}/host/GridLab.PSSX.Module1Name"
      - IMAGE_NAME: "${CI_REGISTRY_IMAGE}/module2"
        DOCKERFILE: "${CI_PROJECT_DIR}/host/GridLab.PSSX.Module2Name/Dockerfile.Module2"
        CONTEXT:    "${CI_PROJECT_DIR}/host/GridLab.PSSX.Module2Name" 
```

## Getting Started

Please note that, depending on the templates used, you must provide certain variables, and you can pass other for configuration purposes.

### Container registry

The internal container registry `registry.gitlab.com` can be used to store container images for the development and build process.

Include the file with to be extended hidden jobs starting with `extend` from this project

```yaml
include:
  - project: 'pss-x/support/gitlab-ci-templates/kaniko'
    ref: v1.0.5 # select desired version here
    file: '/templates/.kaniko-cr.yml'
```

Docker pull can be use a `Personal Access Token` or `Deploy Token`

* Can be useable a dedicated `Personal Access Token` with the scope `read_registry` for Docker registry access via the Internet since write access to the registry is blocked via this route.
  
https://gitlab.com primarily meant to support software developers in versioning, maintaining and building their codebase.
However, for complex development projects, the features offered by the code platform might be insufficient (e.g. full-featured issue boards to support planning in large software development projects).
For those advanced use cases, use different solutions.

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | REGISTRY | x | Registry address | $CI_REGISTRY | 
| V | REGISTRY_USER | x | Current gitlab user | $CI_REGISTRY_USER |
| V | REGISTRY_PASSWORD | x | Current gitlab user password | $CI_REGISTRY_PASSWORD |
| V | RELEASE_REGISTRY |  | Remote registry addres for containers | $CI_REGISTRY |
| V | RELEASE_REGISTRY_USER |  | Remote registry user | $RELEASE_REGISTRY_DEFAULT_USER |
| V | REGISTRY_PASSWORD |  | Remote registry user password | $RELEASE_REGISTRY_DEFAULT_PASSWORD |

$RELEASE_REGISTRY_DEFAULT_USER and $RELEASE_REGISTRY_DEFAULT_PASSWORD are `deploy token` which belongs [releases](https://gitlab.com/pss-x/support/containers/releases) project

Default base address for the container registry to push, pull, or tag project’s images is defined by `$IMAGE_NAME` and `$IMAGE_TAG`

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | IMAGE_NAME | x | Image names must follow this naming convention | registry server/namespace/project | 
| V | IMAGE_TAG  | x | By default the branch or tag name for which project is built. | $CI_COMMIT_REF_NAME |
| V | CR_TF_VAR_NAME |  | It will export the environment variable as **TF_VAR_[value]_image** by manipulating the given value. |  |

NOTE: The same functionality for exporting TF_VAR_image is also applicable in the ECR image pushing part so in order to avoid overwrite problem CR_TF_VAR_NAME and ECR_TF_VAR_NAME variable ONYL be used once in the job definition. Otherwise, the second executed kaniko job overwrites the value assigned by the first executed job.

Kaniko is a tool to build container images from a Dockerfile, inside a container, kaniko is meant to be run as an image: `gcr.io/kaniko-project/executor`

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | DOCKERFILE | x | Path to the dockerfile to be built. | $CI_PROJECT_DIR/Dockerfile | 
| V | CONTEXT | x | Kaniko requires a build context, which represents a directory containing a Dockerfile | $CI_PROJECT_DIR |
| V | EXTRA_ARGUMENTS | | .executor-command can extended via this argument | |

Kaniko cache caches container build artifacts by storing and indexing intermediate layers within a container image registry, such as registry.gitlab.com own artifact registry

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | KANIKO_CACHE_ARGS| | You can pass this value to EXTRA_ARGUMENTS | --cache=true --cache-copy-layers=true --cache-ttl=24h | 

OCI pre-defined annotation keys can be extend or modify via `$IMAGE_LABELS`

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | IMAGE_LABELS | | This specification defines the following annotation keys, intended for but not limited to image index, image manifest, and descriptor authors. | | 

The `dotenv` report collects a set of environment variables as artifacts.

| Variable Name | Description |  
| ------------- | ----------- | 
| IMAGE | "IMAGE_NAME":"IMAGE_TAG"@"IMAGE_DIGEST"  | 
| CR_IMAGE_NAME | The NAME is the general name of the image and will ultimately be the image repository name when and if it is shared. | 
| CR_IMAGE_TAG | The TAG defines a specialized version of the image.  | 
| CR_IMAGE_DIGEST | Immutable identifier. |
| CR_IMAGE | "CR_IMAGE_NAME":"CR_IMAGE_TAG"@"CR_IMAGE_DIGEST" | 

### Azure container registry

Azure Container Registry allows you to build, store, and manage container images and artifacts in a private registry for all types of container deployments.

Include the file with to be extended hidden jobs starting with `extend` from this project

```yaml
include:
  - project: 'pss-x/support/gitlab-ci-templates/kaniko'
    ref: v1.0.5 # select desired version here
    file: '/templates/.kaniko-acr.yml'
```	

You can use an [Azure container registry](https://azure.microsoft.com/en-us/products/container-registry/) to store and manage [Open Container Initiative (OCI)](https://learn.microsoft.com/en-us/azure/container-registry/container-registry-image-formats#oci-artifacts) artifacts as well as Docker and OCI container images.

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | AZURE_ECR | x | Azure Container registery | |

### Amazon ECR private registry

An Amazon ECR private registry hosts our container images in a highly available and scalable architecture.

```yaml
include:
  - project: 'pss-x/support/gitlab-ci-templates/kaniko'
    ref: v1.0.5 # select desired version here
    file: '/templates/.kaniko-ecr.yml'
```

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | AWS_ECR | x | Amazon Elastic Container registery | 843594798263.dkr.ecr.eu-central-1.amazonaws.com/pssx |
| V | AWS_TAG | x | By default combination of project name and the branch or tag name | \$CI_PROJECT_NAME\-\$CI_COMMIT_REF_NAME |
| V | ECR_TF_VAR_NAME | | It will export the environment variable as **TF_VAR_[value]_image** by manipulating the given value. | |

#### Private registry concepts

By default, your account has read and write access to the repositories in your private registry. However,users require permissions to make calls to the Amazon ECR APIs and to push or pull images to and from your private repositories.

#### Private repository policy examples

The following examples show policy statements that you could use to control the permissions that authenticated users have to Amazon ECR repositories.

``` json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:GetRepositoryPolicy",
                "ecr:DescribeRepositories",
                "ecr:ListImages",
                "ecr:DescribeImages",
                "ecr:BatchGetImage",
                "ecr:GetLifecyclePolicy",
                "ecr:GetLifecyclePolicyPreview",
                "ecr:ListTagsForResource",
                "ecr:DescribeImageScanFindings",
                "ecr:InitiateLayerUpload",
                "ecr:UploadLayerPart",
                "ecr:CompleteLayerUpload",
                "ecr:PutImage"
            ],
            "Resource": "*"
        }
    ]
}
```

#### Adding IAM identity permissions (console)

You can use the AWS Management Console to add permissions to an identity (user, user group, or role). To do this, attach managed policies that control permissions, or specify a policy that serves as a permissions boundary. You can also embed an inline policy.

#### Managing access keys for IAM users (Not Recommended)

Access keys are long-term credentials for an IAM user or the AWS account root user. You can use access keys to sign programmatic requests to the AWS CLI or AWS API (directly or using the AWS SDK)

As a [best practice](https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html), use temporary security credentials (IAM roles) instead of creating long-term credentials like access keys, and don't create AWS account root user access keys. We don't recommend generating access keys for your root user, because they allow full access to all your resources for all AWS services, including your billing information. For more information, see Best Practices for AWS accounts(https://docs.aws.amazon.com/accounts/latest/reference/best-practices.html) in the AWS Account Management Reference Guide.

To create an access key:

1. In the Access keys section, choose Create access key. If you already have two access keys, this button is deactivated and you must delete an access key before you can create a new one.

2. On the Access key best practices & alternatives page, choose your use case to learn about additional options which can help you avoid creating a long-term access key. If you determine that your use case still requires an access key, choose Other and then choose Next.

3. (Optional) Set a description tag value for the access key. This adds a tag key-value pair to your IAM user. This can help you identify and rotate access keys later. The tag key is set to the access key id. The tag value is set to the access key description that you specify. When you are finished, choose Create access key.

4. On the Retrieve access keys page, choose either Show to reveal the value of your user's secret access key, or Download .csv file. This is your only opportunity to save your secret access key. After you've saved your secret access key in a secure location, choose Done.

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | AWS_ACCESS_KEY_ID | x | Access key for AWS access |  |
| V | AWS_SECRET_ACCESS_KEY | x | Secret key for AWS access |  |
| V | AWS_SESSION_TOKEN | x | Secret Token for AWS access |  |

#### Assume Role (Recommended)

Returns a set of temporary security credentials that you can use to access AWS resources. These temporary credentials consist of an access key ID, a secret access key, and a security token. 
Typically, you use `AssumeRole` within your account or for cross-account access. For a comparison of AssumeRole with other API operations that produce temporary credentials, see [Requesting Temporary Security Credentials](https://gitlab.com/pss-x/support/gitlab-ci-templates/aws-authenticate)

The [AWS STS cli command](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/sts/assume-role-with-web-identity.html) can be used to assume role via web identity and get credential information

You can use [aws-authenticate](https://gitlab.com/pss-x/support/gitlab-ci-templates/aws-authenticate) template to get credential information

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | ROLE_ARN | x | [Role arn](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference-arns.html) is dedicated role for ci/cd operations | $AWS_DEFAULT_ROLE_ARN |

The `dotenv` report collects a set of environment variables as artifacts.

| Variable Name | Description |
| ------------- | ----------- |
| IMAGE | "IMAGE_NAME":"IMAGE_TAG"@"IMAGE_DIGEST"  |
| ECR_IMAGE_NAME | The NAME is the general name of the image and will ultimately be the image repository name when and if it is shared. | 
| ECR_IMAGE_TAG | The TAG defines a specialized version of the image.  |
| ECR_IMAGE_DIGEST | Immutable identifier. | 
| ECR_IMAGE | "AWS_ECR":"AWS_TAG"@"ECR_IMAGE_DIGEST" |

### Jfrog container registry

The extenal container registry `[dedicated-instance-name].jfrog.io` can be used to store container images for the releases. 

```yaml
include:
  - project: 'pss-x/support/gitlab-ci-templates/kaniko'
    ref: v1.0.5 # select desired version here
    file: '/templates/.kaniko-jf.yml'
```	

The JFrog Container Registry is powered by JFrog Artifactory with a set of features that have been customized to serve the primary purpose of running Docker and Helm packages in a Container Registry.

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | JF_URL | x | Url to publish the artifacts (this url must not have https:// or http://) | $JF_DEFAULT_URL |
| V | JF_USER | x | CI User | $JF_DEFAULT_USER |
| V | JF_PASSWORD | x | CI User Password | $JF_DEFAULT_PASSWORD |
| V | JF_ACCESS_TOKEN | x | Authentication token | $JF_DEFAULT_ACCESS_TOKEN |
| V | JF_REFERENCE_TOKEN | x |  A Reference Token is simply a short string of characters that refers to an actual token. Kaniko authentication uses that one | $JF_DEFAULT_REFERENCE_TOKEN |

Local repositories are physical, locally managed repositories into which you can deploy artifacts.

Artifacts in a local repository can be accessed directly using the following URL:

> http://< host > : < port > / artifactory / < local-repository-name > / < artifact-path >

Local and remote repositories are true physical repositories, while a virtual repository is actually an aggregation of them used to create controlled domains for the search and resolution of artifacts.

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | JF_REPO_KEY | x | Local repository name | $JF_DEFAULT_REPO_KEY |

A unique Project Key that helps you identify and group your projects.

The Platform Administrator first creates and configures the basic Project settings, including the **Project name** and **Project Key**, assigns Project Admins, and grants Project Admins privileges allowing them to manage Platform resources and Project members.

Platform Admins can assign repositories to the projects in this initial stage.

The Platform Administrator is granted all permissions in all projects.

Project Admins can then proceed to grant Platform Users and Groups access to the project as Project Members by granting them Global or Project-level roles.

Project Admins can assign existing Repositories or Pipelines Sources from the other projects to a project and/or create new repositories as part of the project.

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | JF_PROJECT_KEY | | Project key value | |

This is an example showing how to collect build-info, while using the Kaniko container to build an image from a docker file and push it to Artifactory.

```yml
push:buildinfo:
include:
  - project: 'pss-x/support/gitlab-ci-templates/jfrog'
    ref: v1.0.5 # select desired version here
    file: '/templates/.build-info.yml'

push:jf:
  extends: .jf:build-container-image
  stage: push:jf
  needs: [ "version" ] 
  variables:
    IMAGE_NAME: gridlab.pssx/kaniko
    IMAGE_TAG: $VERSION
    JF_REPO_KEY: pssx-basic

push:jf-buildinfo:
  extends: .jf:create-build-info
  stage: push:jf-buildinfo
  needs:
    - job: "push:jf"
      artifacts: true
```

The `dotenv` report collects a set of environment variables as artifacts.

| Variable Name | Description |  
| ------------- | ----------- | 
| IMAGE | "IMAGE_NAME":"IMAGE_TAG"@"IMAGE_DIGEST"  |  
| JF_IMAGE_NAME | The NAME is the general name of the image and will ultimately be the image repository name when and if it is shared. | 
| JF_IMAGE_TAG | The TAG defines a specialized version of the image.  | 
| JF_IMAGE_DIGEST | Immutable identifier. |
| JF_IMAGE | "JF_IMAGE_NAME":"JF_IMAGE_TAG"@"JF_IMAGE_DIGEST" |
| JF_REPO_KEY | Local repository name. | 
| JF_PROJECT_KEY | Project key value. | 
| JF_IMAGE_FILE_DIRECTORY | File path where kaniko image-details files are collected |
| JF_BUILD_NUMBER_FILE_DIRECTORY | File path where ci job ids are collected |

## Patching the Container with `root` User

If your container is running with a user which does not perform package management operations (`apt-get install ...`), then you might need to create a temporary wrapper (a patched container) with the `root` user.

Include the file with to be extended hidden jobs `extend.gitlab-ci.yml` from this project and use via `extends` in your job:

```yml
use-kaniko:
  extends: .cr:build-rooted-container-image
```

This will be create container with following format `${IMAGE_NAME}:${IMAGE_TAG}-${CI_PIPELINE_ID}-rooted` by using [Dockerfile.Clearing](./Dockerfile.Clearing) file.

The default format can be changed with the `$DESTINATION` variable.

```yml
include:
  - project: 'pss-x/support/gitlab-ci-templates/kaniko'
    ref: v1.0.5 # select desired version here
    file: '/templates/.kaniko.yml'

stages:
  - push:cr
  - push:cr:root

push:cr:
  extends: .cr:build-container-image
  stage: push:cr
  when: manual
  variables:
    DOCKERFILE: "$CI_PROJECT_DIR/Dockerfile"
 
push:cr:rooted:
  extends: .cr:build-rooted-container-image
  stage: push:cr:root
  variables:
    DOCKERFILE: "$CI_PROJECT_DIR/Dockerfile.Clearing"
    DESTINATION: "${IMAGE_NAME}:${IMAGE_TAG}-${CI_PIPELINE_ID}-rooted"
```

| Variable/File | Variable Name | Required | Description | Default Value | 
| ------------- | ------------- | -------- | ----------- | -------- |
| V | DESTINATION | | Rooted container registry path | \${IMAGE_NAME}:\${IMAGE_TAG}\-\${CI_PIPELINE_ID}-rooted |

## Linting dockerfile

You can run `hadolint` to lint your Dockerfile

Include the file with to be extended hidden jobs `extend.gitlab-ci.yml` from this project and use via `extends` in your job:

```yml
use-hadolint:
  extends: .hadolint
```

### Gitlab Variables

Depending on the template files used, there are required and optional variables. These can either be defined via ```.gitlab-ci.yaml``` or set as gitlab secret variables.
In addition, to the variables mentioned above, the following variables are needed, which are typically provided via Gitlab secret variables.

Please note that in combination with other templates gitlab secret variables may become necessary.
This is especially the case if you use the release template. For more information, check the corresponding
[README.md](https://gitlab.com/pss-x/support/gitlab-ci-templates/releases#gitlab-secret-variables).

## Running kaniko in Docker

Requirements:

- [Docker](https://docs.docker.com/install/)

We can run the kaniko executor image locally in a Docker daemon to build and push an image from a Dockerfile.

Run Kaniko in Docker: Execute the following command to build the container image using Kaniko:

```shell
docker run \
	-v $(pwd):/build \
	gcr.io/kaniko-project/executor:latest \
	--dockerfile Dockerfile \
	--tar-path /build/image.tar \
	--no-push \
	--context /build \
    --destination myimage:1.0
```

```powershell
docker run --rm -v .:/build -w /build gcr.io/kaniko-project/executor:latest --tar-path /build/image.tar --dockerfile Dockerfile --context dir:///build --no-push --destination myimage:1.0 --digest-file /kaniko/image.digest --image-name-tag-with-digest-file /kaniko/image.detail  
```

## Have a Problem or Suggestion ?

Please see a list of features and bugs under issues If you have a problem which is not listed there, please let us known.

