# Semantic Versioning Changelog

## [1.0.5](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/compare/v1.0.4...v1.0.5) (2025-02-26)


### Bug Fixes

* rebranding applied [skip ci] ([8e241ed](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/commit/8e241edcc1b0c0a1b1632b63f3ddf3ed9f0905d2))
* renovate json updated [skip ci] ([370d5d1](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/commit/370d5d11751e7b97e417cdedfa9aaad0695cf3b5))
* renovate json updated [skip ci] ([47583b0](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/commit/47583b0b07d630ee42fca5fe0fd19d428039afc3))

## [1.0.4](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/compare/v1.0.3...v1.0.4) (2025-02-19)


### Bug Fixes

* rebranding applied [skip ci] ([84d7c33](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/commit/84d7c334ac0344d43c3341f93089e9ccabb1732a))

## [1.0.3](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/compare/v1.0.2...v1.0.3) (2025-02-19)


### Bug Fixes

* rebranding applied ([19b9de2](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/commit/19b9de21a2c2ba9304e2c24ee9ca0f3f21a1191d))

## [1.0.2](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/compare/v1.0.1...v1.0.2) (2025-02-16)


### Bug Fixes

* missing semicolon added while exporting IMAGE_DIGEST ([ef73749](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/commit/ef7374916845d7a5fe1a2e09abd5bbef1233b31e))

## [1.0.1](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/compare/v1.0.0...v1.0.1) (2025-02-16)


### Bug Fixes

* hadolint error fixed ([788a257](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/commit/788a257a1712540ce095956252cd731ab273b0ee))
* missing test jobs added ([bb1d3e3](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/commit/bb1d3e314c4766ce1ba51870b85217005684eb97))

# 1.0.0 (2025-02-16)


### Bug Fixes

* image_digest env variable error fixed ([f7e1e63](https://gitlab.com/pss-x/support/gitlab-ci-templates/kaniko/commit/f7e1e63fad7c337e711e42ada07e099369e627cc))
