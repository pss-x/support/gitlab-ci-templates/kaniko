# Copyright 2025, GridLab
# Licensed under the GNU Lesser General Public License (LGPL)

include:
  - local: '/templates/.configuration.yml'

.default-value: &default |
  if [[ -z "${IMAGE_NAME}" ]]; then
    echo -e "\e[33mWarning: Variable IMAGE_NAME is not set, using default value..."
    IMAGE_NAME=$CI_REGISTRY_IMAGE
  fi
  if [[ -z "${IMAGE_TAG}" ]]; then
    echo -e "\e[33mWarning: Variable IMAGE_TAG is not set, using default value..."
    IMAGE_TAG=$CI_COMMIT_REF_NAME
  fi
 
# https://github.com/GoogleContainerTools/kaniko/issues/687#issuecomment-1033913373
# echo "{\"auths\":{\"$REGISTRY\":{\"username\":\"$REGISTRY_USER\",\"password\":\"$REGISTRY_PASSWORD\"},\"$RELEASE_REGISTRY\":{\"username\":\"$DEPLOY_USER\",\"password\":\"$DEPLOY_PASSWORD\"}}}" > /kaniko/.docker/config.json  
.push-cr: &push-cr |
  echo "Configuring kaniko to push images into the container registry"
  IMAGE_TAG=$(echo ${IMAGE_TAG} | sed 's/\//-/g')
  echo "Current image tag is ${IMAGE_TAG}"
  mkdir -p /kaniko
  cat > /kaniko/image.digest
  rm -rf /kaniko/.docker/config.json
  mkdir -p /kaniko/.docker/
  echo "{\"auths\":{\"$REGISTRY\":{\"username\":\"$REGISTRY_USER\",\"password\":\"$REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
  
.executor-command: &cr-command
  - /kaniko/executor
    --context $CONTEXT
    --digest-file /kaniko/image.digest
    --dockerfile $DOCKERFILE
    --destination ${IMAGE_NAME}:${IMAGE_TAG}
    --build-arg GITLAB_NUGET_PASSWORD=$GITLAB_NUGET_PASSWORD 
    --build-arg GITLAB_NUGET_USERNAME=$GITLAB_NUGET_USERNAME 
    --build-arg GITLAB_NPM_PASSWORD=$GITLAB_NPM_PASSWORD
    --build-arg ABP_USERNAME=$ABP_USERNAME
    --build-arg ABP_PASSWORD=$ABP_PASSWORD
    $IMAGE_LABELS $EXTRA_ARGUMENTS
  
.executor-command: &cr-root-command
  - /kaniko/executor
    --context $CONTEXT
    --digest-file /kaniko/image.digest
    --dockerfile $DOCKERFILE
    --destination ${DESTINATION}
    --build-arg GITLAB_NUGET_PASSWORD=$GITLAB_NUGET_PASSWORD
    --build-arg GITLAB_NUGET_USERNAME=$GITLAB_NUGET_USERNAME
    --build-arg GITLAB_NPM_PASSWORD=$GITLAB_NPM_PASSWORD
    --build-arg ABP_USERNAME=$ABP_USERNAME
    --build-arg ABP_PASSWORD=$ABP_PASSWORD
    --build-arg IMAGE=${IMAGE_NAME}:${IMAGE_TAG}
    $EXTRA_ARGUMENTS

 # use `export` cmd to check every env variable
.output-environment: &environment |
  IMAGE_DIGEST=$(cat /kaniko/image.digest)
  IMAGE=${IMAGE_NAME}:${IMAGE_TAG}@${IMAGE_DIGEST}
  export IMAGE=$IMAGE
  echo "IMAGE=$IMAGE" > kaniko-cr.env
  if [[ ! -z "${IMAGE}" ]]; then { export CR_IMAGE=$IMAGE; echo "CR_IMAGE=$CR_IMAGE" >> kaniko-cr.env; }; fi
  if [[ ! -z "${IMAGE_NAME}" ]]; then { export CR_IMAGE_NAME=$IMAGE_NAME; echo "CR_IMAGE_NAME=$CR_IMAGE_NAME" >> kaniko-cr.env; }; fi
  if [[ ! -z "${IMAGE_TAG}" ]]; then { export CR_IMAGE_TAG=$IMAGE_TAG; echo "CR_IMAGE_TAG=$CR_IMAGE_TAG" >> kaniko-cr.env; }; fi
  if [[ ! -z "${IMAGE_DIGEST}" ]]; then { export CR_IMAGE_DIGEST=$IMAGE_DIGEST; echo "CR_IMAGE_DIGEST=$CR_IMAGE_DIGEST" >> kaniko-cr.env; }; fi
  cat kaniko-cr.env

.output-terraform: &tf-vars
  - |
    if [[ "${CR_TF_VAR_NAME}" != "" ]] || [[ ! -z "${CR_TF_VAR_NAME}" ]]; then
      TEMP_VAR_NAME=$(echo ${CR_TF_VAR_NAME} | tr '[:upper:]' '[:lower:]')
      TEMP_VAR_NAME=$(echo ${TEMP_VAR_NAME} | sed -e 's/[][\\^&#@!*%+.$-]/\_/g')
      echo "Current environment variable name is TF_VAR_${TEMP_VAR_NAME}"
      export "TF_VAR_${TEMP_VAR_NAME}=${IMAGE_NAME}:${IMAGE_TAG}"
      echo "TF_VAR_${TEMP_VAR_NAME}=${IMAGE_NAME}:${IMAGE_TAG}" >> kaniko-cr.env
    fi
  
.cr:build-container-image:
  extends: .kaniko-executor
  variables: 
    IMAGE_NAME: $CI_REGISTRY_IMAGE
    IMAGE_TAG: $CI_COMMIT_REF_NAME
    CR_TF_VAR_NAME: ""
  before_script:
    - *default
    - *push-cr
  script:
    - echo "Pushing $IMAGE_NAME image to $CI_REGISTRY registry"
    - !reference [.image-label] 
    - *cr-command
    - *environment
    - *tf-vars 
    - cat kaniko-cr.env
  artifacts:
    reports:
      dotenv: kaniko-cr.env
    expire_in: never
  rules:
    - !reference [.kaniko-workflow-rule, rules]
  allow_failure: false
  retry:
    max: 2
    when: always
     
.cr:build-rooted-container-image:
  extends: .kaniko-executor
  variables: 
    IMAGE_NAME: $CI_REGISTRY_IMAGE
    IMAGE_TAG: $CI_COMMIT_REF_NAME
    DESTINATION: ${IMAGE_NAME}:${IMAGE_TAG}-${CI_PIPELINE_ID}-rooted
  before_script:
    - *default
    - *push-cr
  script:
    - echo "Changing user of the $IMAGE_NAME image with Dockerfile.Clearing file"
    - *cr-root-command
    - *environment
  artifacts:
    reports:
      dotenv: kaniko-cr.env
    expire_in: never
  rules:     
    - !reference [.kaniko-workflow-rule, rules]
  allow_failure: false
  retry:
    max: 2
    when: always

.cr:release-container-image:
  image: alpine:latest@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c
  variables:
    IMAGE_NAME: $CI_REGISTRY_IMAGE
    IMAGE_TAG: $CI_COMMIT_REF_NAME
    # Default registry parameters, those values can be overriden
    REGISTRY: $CI_REGISTRY
    REGISTRY_USER: $CI_REGISTRY_USER
    REGISTRY_PASSWORD: $CI_REGISTRY_PASSWORD
    # Release registry
    RELEASE_REGISTRY: $CI_REGISTRY
    RELEASE_REGISTRY_USER: $RELEASE_REGISTRY_DEFAULT_USER
    RELEASE_REGISTRY_PASSWORD: $RELEASE_REGISTRY_DEFAULT_PASSWORD
  before_script:
    - apk update && apk add skopeo
    - *default
  script:
    - export IMAGE_NAMESPACE=$(echo $CI_PROJECT_TITLE | tr " " "_")
    - echo "$IMAGE_NAME:$IMAGE_TAG ===> registry.gitlab.com/pss-x/support/containers/releases/$IMAGE_NAMESPACE:$IMAGE_TAG"
    - skopeo copy --src-creds $REGISTRY_USER:$REGISTRY_PASSWORD --dest-creds $RELEASE_REGISTRY_USER:$RELEASE_REGISTRY_PASSWORD docker://${IMAGE_NAME}:${IMAGE_TAG} docker://registry.gitlab.com/pss-x/support/containers/releases/${IMAGE_NAMESPACE}:${IMAGE_TAG}
  rules:     
    - if: '$CI_COMMIT_TAG'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CI_COMMIT_REF_PROTECTED == "true" && $CI_COMMIT_REF_NAME !~ /^renovate\/.*/ && $CI_COMMIT_BRANCH !~ /^integration\/.*/ && $CI_COMMIT_BRANCH !~ /^infrastructure\/.*/'
      when: on_success
    - if: '$CI_COMMIT_BRANCH && $CI_COMMIT_REF_PROTECTED == "true" && $CI_COMMIT_REF_NAME !~ /^renovate\/.*/'
      when: on_success 
  allow_failure: false
  retry:
    max: 2
    when: always